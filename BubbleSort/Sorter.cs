﻿using System;

// ReSharper disable InconsistentNaming
#pragma warning disable SA1611

namespace BubbleSort
{
    public static class Sorter
    {
        public static void BubbleSort(this int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (int i = 0, buffer; i < array.Length - 1; i++)
            {
                for (int j = 0; j < array.Length - i - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        buffer = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = buffer;
                    }
                }
            }
        }

        public static void RecursiveBubbleSort(this int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            Sorting(array, array.Length);
        }

        public static void Sorting(int[] array, int n)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (n <= 1)
            {
                return;
            }

            for (int i = 0, buffer; i < n - 1; i++)
            {
                if (array[i] > array[i + 1])
                {
                    buffer = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buffer;
                }
            }

            Sorting(array, n - 1);
        }
    }
}
